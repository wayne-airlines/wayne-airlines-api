package com.jet.practice.WayneAirlinesAPI.entity;

public class Carrier {

	private Integer id;
	private String code;
	private String name;
	private Airport airport;
	private CarrierType carrierType;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Airport getAirport() {
		return airport;
	}

	public void setAirport(Airport airport) {
		this.airport = airport;
	}

	public CarrierType getCarrierType() {
		return carrierType;
	}

	public void setCarrierType(CarrierType carrierType) {
		this.carrierType = carrierType;
	}

	@Override
	public String toString() {
		return "Carrier [id=" + id + ", code=" + code + ", name=" + name + ", airport=" + airport + ", carrierType="
				+ carrierType + "]";
	}

}
