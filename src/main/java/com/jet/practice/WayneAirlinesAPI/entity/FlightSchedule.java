package com.jet.practice.WayneAirlinesAPI.entity;

import java.util.Date;

public class FlightSchedule {

	private Integer id;
	private String code;
	private Airport originAirport;
	private Airport destinationAirport;
	private Date flightDateStart;
	private Date flightDateFinish;
	private Carrier carrier;
	private Integer currentCapacity;
	private String type;
	private String status;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Airport getOriginAirport() {
		return originAirport;
	}

	public void setOriginAirport(Airport originAirport) {
		this.originAirport = originAirport;
	}

	public Airport getDestinationAirport() {
		return destinationAirport;
	}

	public void setDestinationAirport(Airport destinationAirport) {
		this.destinationAirport = destinationAirport;
	}

	public Date getFlightDateStart() {
		return flightDateStart;
	}

	public void setFlightDateStart(Date flightDateStart) {
		this.flightDateStart = flightDateStart;
	}

	public Date getFlightDateFinish() {
		return flightDateFinish;
	}

	public void setFlightDateFinish(Date flightDateFinish) {
		this.flightDateFinish = flightDateFinish;
	}

	public Carrier getCarrier() {
		return carrier;
	}

	public void setCarrier(Carrier carrier) {
		this.carrier = carrier;
	}

	public Integer getCurrentCapacity() {
		return currentCapacity;
	}

	public void setCurrentCapacity(Integer currentCapacity) {
		this.currentCapacity = currentCapacity;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "FlightSchedule [id=" + id + ", code=" + code + ", originAirport=" + originAirport
				+ ", destinationAirport=" + destinationAirport + ", flightDateStart=" + flightDateStart
				+ ", flightDateFinish=" + flightDateFinish + ", carrier=" + carrier + ", currentCapacity="
				+ currentCapacity + ", type=" + type + ", status=" + status + "]";
	}

}
