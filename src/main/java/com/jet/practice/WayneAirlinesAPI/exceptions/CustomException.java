package com.jet.practice.WayneAirlinesAPI.exceptions;

import org.springframework.web.client.RestClientException;

public class CustomException extends RestClientException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7389879479864974562L;

	public CustomException(ExceptionEnum exceptionEnum) {
		super(exceptionEnum.code()+": "+exceptionEnum.message());
		
	}

}
