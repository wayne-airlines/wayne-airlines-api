package com.jet.practice.WayneAirlinesAPI.mapper;

import java.util.List;

import com.jet.practice.WayneAirlinesAPI.entity.Airport;

public interface AirportMapper{

	List<Airport> getAllAirportsByCountry();
	Airport getAirportByCode(String code);
	List<Airport> getAirportByCountryCode(String code);
	void createAirport(String code, String name, String countryCode);
	
}
