package com.jet.practice.WayneAirlinesAPI.mapper;

import com.jet.practice.WayneAirlinesAPI.entity.Carrier;

public interface CarrierMapper {
	
	Carrier getCarrierByCode(String code);

}
