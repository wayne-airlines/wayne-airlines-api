package com.jet.practice.WayneAirlinesAPI.mapper;

import java.util.Date;
import java.util.List;

import com.jet.practice.WayneAirlinesAPI.entity.FlightSchedule;

public interface FlightScheduleMapper {
	
	void createFlightSchedule(String flightScheduleCode, String originAirport, String destinationAirport,
			Date flightDateStart, Date flightDateFinish, String carrier, Integer currentCapacity,
			String flightType, String status);
	
	FlightSchedule getFlightScheduleByCode(String code);
	
	List<FlightSchedule> getFlightScheduleByCarrierCode(String carrierCode);
	
	FlightSchedule getLatestFlightSchedule(String carrierCcode);
	
	List<FlightSchedule> getFlightScheduleByFlightStartDate(String flightDateStart);

}
