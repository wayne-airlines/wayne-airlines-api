package com.jet.practice.WayneAirlinesAPI.mapper;

import com.jet.practice.WayneAirlinesAPI.entity.CarrierType;

public interface CarrierTypeMapper {
	
	CarrierType getCarrierTypeByCode(String code);

}
