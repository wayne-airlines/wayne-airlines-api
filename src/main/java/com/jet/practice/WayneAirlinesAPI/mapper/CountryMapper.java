package com.jet.practice.WayneAirlinesAPI.mapper;

import com.jet.practice.WayneAirlinesAPI.entity.Country;

public interface CountryMapper {
	
	Country getCountryByCode(String code);

}
