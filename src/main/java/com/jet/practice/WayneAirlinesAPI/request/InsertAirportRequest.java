package com.jet.practice.WayneAirlinesAPI.request;

import io.swagger.annotations.ApiModel;

@ApiModel(value = "InsertAirportRequest", description = "insert Airport Request")
public class InsertAirportRequest {
	
	private String countryCode;
	private String airportCode;
	private String airportName;

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getAirportCode() {
		return airportCode;
	}

	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	public String getAirportName() {
		return airportName;
	}

	public void setAirportName(String airportName) {
		this.airportName = airportName;
	}

	@Override
	public String toString() {
		return "InsertAirportRequest [countryCode=" + countryCode + ", airportCode=" + airportCode + ", airportName="
				+ airportName + "]";
	}

}
