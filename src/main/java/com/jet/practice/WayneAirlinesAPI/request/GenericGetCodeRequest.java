package com.jet.practice.WayneAirlinesAPI.request;

import io.swagger.annotations.ApiModel;

@ApiModel(value = "GenericGetCodeRequest", description = "get by code request")
public class GenericGetCodeRequest {

	private String code;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String toString() {
		return "GetAirportByCodeRequest [code=" + code + "]";
	}

}
