package com.jet.practice.WayneAirlinesAPI.service;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jet.practice.WayneAirlinesAPI.entity.Airport;
import com.jet.practice.WayneAirlinesAPI.entity.Country;
import com.jet.practice.WayneAirlinesAPI.exceptions.CustomException;
import com.jet.practice.WayneAirlinesAPI.exceptions.ExceptionEnum;
import com.jet.practice.WayneAirlinesAPI.mapper.AirportMapper;
import com.jet.practice.WayneAirlinesAPI.request.InsertAirportRequest;
import com.jet.practice.WayneAirlinesAPI.response.GenericCodeResponse;

@Service
@Transactional
public class AirportServiceImpl implements AirportService {

	@Autowired
	private AirportMapper airportMapper;
	
	@Autowired
	private CountryService countryService;
	
	@Override
	public List<Airport> getAllAirportsByCountry() {
		return airportMapper.getAllAirportsByCountry();
	}

	@Override
	public Airport getAirportByCode(String code) {
		return airportMapper.getAirportByCode(code);
	}

	@Override
	public List<Airport> getAirportByCountryCode(String code) {
		return airportMapper.getAirportByCountryCode(code);
	}

	@Override
	public GenericCodeResponse createAirport(InsertAirportRequest request) {
		String airportCode = request.getAirportCode();
		String airportName = request.getAirportName();
		String countryCode = request.getCountryCode();
		GenericCodeResponse response = new GenericCodeResponse();
		
		//check if request is complete
		if(StringUtils.isEmpty(airportName) || StringUtils.isEmpty(airportCode) || StringUtils.isEmpty(countryCode)){
			throw new CustomException(ExceptionEnum.MISSING_PARAMETER_ERROR);
		}
		
		//Check if country code is existing
		Country country = countryService.getCountryByCode(countryCode);
		if(country == null) {
			throw new CustomException(ExceptionEnum.COUNTRY_NOT_EXISTING_ERROR);
		}
		
		//Check for duplicates
		Airport airport = airportMapper.getAirportByCode(airportCode);
		if(airport!=null) {
			throw new CustomException(ExceptionEnum.AIRPORT_ALREADY_EXISTING_ERROR);
		}
		
		try {
			airportMapper.createAirport(airportCode, airportName, countryCode);
			response.setCode(airportCode);
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return response;
		
	}

}
