package com.jet.practice.WayneAirlinesAPI.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jet.practice.WayneAirlinesAPI.entity.User;
import com.jet.practice.WayneAirlinesAPI.mapper.UserMapper;

@Service
@Transactional
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserMapper userMapper;

	@Override
	public User getUserByUsername(String username) {
		return userMapper.getUserByUsername(username);
	}

}
