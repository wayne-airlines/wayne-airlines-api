package com.jet.practice.WayneAirlinesAPI.service;

import java.util.List;

import com.jet.practice.WayneAirlinesAPI.entity.Airport;
import com.jet.practice.WayneAirlinesAPI.request.InsertAirportRequest;
import com.jet.practice.WayneAirlinesAPI.response.GenericCodeResponse;

public interface AirportService {

	List<Airport> getAllAirportsByCountry();

	Airport getAirportByCode(String code);
	
	List<Airport> getAirportByCountryCode(String code);
	
	GenericCodeResponse createAirport(InsertAirportRequest request);
}
