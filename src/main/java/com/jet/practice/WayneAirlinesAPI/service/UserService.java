package com.jet.practice.WayneAirlinesAPI.service;

import com.jet.practice.WayneAirlinesAPI.entity.User;

public interface UserService {

	User getUserByUsername(String username);

}
