package com.jet.practice.WayneAirlinesAPI.service;

import com.jet.practice.WayneAirlinesAPI.entity.Country;

public interface CountryService {
	
	Country getCountryByCode(String code);
	

}
