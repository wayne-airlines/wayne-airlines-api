package com.jet.practice.WayneAirlinesAPI.service;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jet.practice.WayneAirlinesAPI.entity.Airport;
import com.jet.practice.WayneAirlinesAPI.entity.Carrier;
import com.jet.practice.WayneAirlinesAPI.entity.FlightSchedule;
import com.jet.practice.WayneAirlinesAPI.exceptions.CustomException;
import com.jet.practice.WayneAirlinesAPI.exceptions.ExceptionEnum;
import com.jet.practice.WayneAirlinesAPI.mapper.FlightScheduleMapper;
import com.jet.practice.WayneAirlinesAPI.request.InsertFlightScheduleRequest;
import com.jet.practice.WayneAirlinesAPI.response.GenericCodeResponse;

@Service
@Transactional
public class FlightScheduleServiceImpl implements FlightScheduleService {

	@Autowired
	private FlightScheduleMapper flightScheduleMapper;

	@Autowired
	private CarrierService carrierService;

	@Autowired
	private AirportService airportService;

	@Override
	public GenericCodeResponse createFlightSchedule(InsertFlightScheduleRequest request) {

		String flightScheduleCode = request.getFlightScheduleCode();
		String originAirport = request.getOriginAirportCode();
		String destinationAirport = request.getDestinationAirportCode();
		Date flightDateStart = request.getFlightDateStart();
		Date flightDateFinish = request.getFlightDateFinish();
		String carrier = request.getCarrierCode();
		Integer currentCapacity = request.getCurrentCapacity();
		String flightType = request.getFlightType();
		String status = request.getStatus();

		// Check validity of parameters
		if (StringUtils.isEmpty(flightScheduleCode)) {
			throw new CustomException(ExceptionEnum.MISSING_PARAMETER_ERROR);
		} else {
			FlightSchedule flightSchedule = flightScheduleMapper.getFlightScheduleByCode(flightScheduleCode);
			if (flightSchedule != null) {
				throw new CustomException(ExceptionEnum.FLIGHT_SCHEDULE_EXISTING_ERROR);
			}
		}

		if (StringUtils.isEmpty(originAirport)) {
			throw new CustomException(ExceptionEnum.MISSING_PARAMETER_ERROR);
		} else {
			Airport airportOfOrigin = airportService.getAirportByCode(originAirport);
			if (airportOfOrigin == null) {
				throw new CustomException(ExceptionEnum.AIRPORT_DOES_NOT_EXIST_ERROR);
			}
		}

		if (StringUtils.isEmpty(destinationAirport)) {
			throw new CustomException(ExceptionEnum.MISSING_PARAMETER_ERROR);
		} else {
			Airport airportOfDestination = airportService.getAirportByCode(destinationAirport);
			if (airportOfDestination == null) {
				throw new CustomException(ExceptionEnum.AIRPORT_DOES_NOT_EXIST_ERROR);
			}
		}

		if (StringUtils.isEmpty(carrier)) {
			throw new CustomException(ExceptionEnum.MISSING_PARAMETER_ERROR);
		} else {
			Carrier carrierScheduled = carrierService.getCarrierByCode(carrier);
			if (carrierScheduled == null) {
				throw new CustomException(ExceptionEnum.CARRIER_DOES_NOT_EXIST_ERROR);
			}

			if (currentCapacity > carrierScheduled.getCarrierType().getCapacity()) {
				throw new CustomException(ExceptionEnum.FLIGHT_OVER_CAPACITY_ERROR);
			}
		}

		checkFlightScheduleValidity(request);

		GenericCodeResponse response = new GenericCodeResponse();

		try {
			flightScheduleMapper.createFlightSchedule(flightScheduleCode, originAirport, destinationAirport,
					flightDateStart, flightDateFinish, carrier, currentCapacity, flightType, status);
			response.setCode(request.getFlightScheduleCode());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return response;
	}

	@Override
	public FlightSchedule getFlightScheduleByCode(String code) {
		return flightScheduleMapper.getFlightScheduleByCode(code);
	}

	@Override
	public List<FlightSchedule> getFlightScheduleByCarrierCode(String carrierCode) {
		return flightScheduleMapper.getFlightScheduleByCarrierCode(carrierCode);
	}

	@Override
	public FlightSchedule getLatestFlightSchedule(String carrierCcode) {
		return flightScheduleMapper.getLatestFlightSchedule(carrierCcode);
	}

	@Override
	public List<FlightSchedule> getFlightScheduleByFlightStartDate(String flightDateStart) {
		return flightScheduleMapper.getFlightScheduleByFlightStartDate(flightDateStart);
	}

	
	//TODO too messy refactor the shit out of this, maybe factory pattern works?
	private void checkFlightScheduleValidity(InsertFlightScheduleRequest request) {
		String flightScheduleCode = request.getFlightScheduleCode();
		String originAirport = request.getOriginAirportCode();
		String destinationAirport = request.getDestinationAirportCode();
		Date flightDateStart = request.getFlightDateStart();
		Date flightDateFinish = request.getFlightDateFinish();
		String carrier = request.getCarrierCode();
		String status = request.getStatus();

		if (flightDateStart.after(flightDateFinish)) {
			throw new CustomException(ExceptionEnum.INVALID_FLIGHT_DATE_START_ERROR);
		}

		if (flightDateFinish.before(flightDateStart)) {
			throw new CustomException(ExceptionEnum.INVALID_FLIGHT_DATE_FINISH_ERROR);
		}

		// Check if conflicting with flight currently saved
		DateTime dateToQuery = new DateTime(flightDateStart);
		List<FlightSchedule> flightList = getFlightScheduleByFlightStartDate(dateToQuery.toString("yyyy-MM-dd"));
		for (FlightSchedule flightSchedule : flightList) {

			LocalTime beforeTimeStart = new LocalTime(flightDateStart.getTime());
			LocalTime savedBeforeTimeStart = new LocalTime(flightSchedule.getFlightDateStart().getTime());
			int datesCompareResult = beforeTimeStart.compareTo(savedBeforeTimeStart);
			if (datesCompareResult == 0 || datesCompareResult == -1) {// compare time
				if (StringUtils.equals(originAirport, flightSchedule.getOriginAirport().getCode())) {
					throw new CustomException(ExceptionEnum.CONFLICTING_FLIGHT_START_ERROR);
				}
			}

			/*
			 * LocalTime beforeTimeFinish = new LocalTime(flightDateFinish.getTime());
			 * LocalTime savedBeforeTimeFinish = new
			 * LocalTime(flightSchedule.getFlightDateFinish().getTime());
			 * if(beforeTimeFinish.compareTo(savedBeforeTimeFinish) == 0) { if
			 * (StringUtils.equals(destinationAirport,
			 * flightSchedule.getDestinationAirport().getCode())) { throw new
			 * CustomException(ExceptionEnum.CONFLICTING_FLIGHT_FINISH_ERROR); } }
			 */

		}

		// get latest flightEnd details
		if (!flightList.isEmpty()) {
			FlightSchedule latestFlightSchedule = Collections.max(flightList,
					Comparator.comparing(f -> f.getFlightDateFinish()));
			// TODO check schedule
			if (latestFlightSchedule != null) {
				if (!StringUtils.equals(latestFlightSchedule.getStatus(), "FINISHED")) {
					if (flightDateStart.before(latestFlightSchedule.getFlightDateFinish())) {
						throw new CustomException(ExceptionEnum.FLIGHT_NOT_FINISH_ERROR);
					} else {
						Long interval = latestFlightSchedule.getFlightDateFinish().getTime()
								- flightDateStart.getTime();
						if (interval < 2) {
							throw new CustomException(ExceptionEnum.FLIGHT_SCHEDULE_INTERVAL_ERROR);
						}
					}
				}
			}
		}
		// get flight counter check how many, last one should be home airport
	}

}
