package com.jet.practice.WayneAirlinesAPI.service;

import java.util.List;

import com.jet.practice.WayneAirlinesAPI.entity.FlightSchedule;
import com.jet.practice.WayneAirlinesAPI.request.InsertFlightScheduleRequest;
import com.jet.practice.WayneAirlinesAPI.response.GenericCodeResponse;

public interface FlightScheduleService {

	GenericCodeResponse createFlightSchedule(InsertFlightScheduleRequest request);

	FlightSchedule getFlightScheduleByCode(String code);
	
	List<FlightSchedule> getFlightScheduleByCarrierCode(String carrierCode);
	
	FlightSchedule getLatestFlightSchedule(String carrierCcode);
	
	List<FlightSchedule> getFlightScheduleByFlightStartDate(String flightDateStart);

}
