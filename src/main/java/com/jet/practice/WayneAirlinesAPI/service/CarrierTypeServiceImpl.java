package com.jet.practice.WayneAirlinesAPI.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jet.practice.WayneAirlinesAPI.entity.CarrierType;
import com.jet.practice.WayneAirlinesAPI.mapper.CarrierTypeMapper;

@Service
@Transactional
public class CarrierTypeServiceImpl implements CarrierTypeService {

	@Autowired
	private CarrierTypeMapper carrierTypeMapper;
	
	@Override
	public CarrierType getCarrierTypeByCode(String code) {
		return carrierTypeMapper.getCarrierTypeByCode(code);
	}

}
