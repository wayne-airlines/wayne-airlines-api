package com.jet.practice.WayneAirlinesAPI.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jet.practice.WayneAirlinesAPI.entity.Country;
import com.jet.practice.WayneAirlinesAPI.mapper.CountryMapper;
@Service
@Transactional
public class CountryServiceImpl implements CountryService {
	
	@Autowired
	private CountryMapper countryMapper;

	@Override
	public Country getCountryByCode(String code) {
		return countryMapper.getCountryByCode(code);
	}

}
