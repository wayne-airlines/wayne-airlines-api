package com.jet.practice.WayneAirlinesAPI.response;

import io.swagger.annotations.ApiModel;

@ApiModel(value = "GenericCodeResponse", description = "get by code response")
public class GenericCodeResponse {

	private String code;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String toString() {
		return "GenericCodeResponse [code=" + code + "]";
	}

}
