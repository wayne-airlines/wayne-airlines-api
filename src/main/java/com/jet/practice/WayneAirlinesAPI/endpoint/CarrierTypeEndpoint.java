package com.jet.practice.WayneAirlinesAPI.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jet.practice.WayneAirlinesAPI.entity.CarrierType;
import com.jet.practice.WayneAirlinesAPI.request.GenericGetCodeRequest;
import com.jet.practice.WayneAirlinesAPI.service.CarrierTypeService;

@RestController
@RequestMapping("/api")
public class CarrierTypeEndpoint {
	
	@Autowired
	private CarrierTypeService carrierTypeService;
	
	@PostMapping(value="/getCarrierTypeByCode", produces="application/json")
	private CarrierType getCarrierTypeByCode(@RequestBody GenericGetCodeRequest request) {
		return carrierTypeService.getCarrierTypeByCode(request.getCode());
		
	}

}
