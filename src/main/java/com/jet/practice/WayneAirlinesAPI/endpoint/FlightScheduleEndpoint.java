package com.jet.practice.WayneAirlinesAPI.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jet.practice.WayneAirlinesAPI.entity.FlightSchedule;
import com.jet.practice.WayneAirlinesAPI.request.GenericGetCodeRequest;
import com.jet.practice.WayneAirlinesAPI.request.InsertFlightScheduleRequest;
import com.jet.practice.WayneAirlinesAPI.response.GenericCodeResponse;
import com.jet.practice.WayneAirlinesAPI.service.FlightScheduleService;

@RestController
@RequestMapping("/api")
public class FlightScheduleEndpoint {
	
	@Autowired
	private FlightScheduleService flightScheduleService;
	
	@PostMapping(value="/getFlightScheduleByCode", produces="application/json")
	private FlightSchedule getFlightScheduleByCode(@RequestBody GenericGetCodeRequest request) {
		return flightScheduleService.getFlightScheduleByCode(request.getCode());
		
	}
	
	@PostMapping(value="/createFlightSchedule", produces="application/json")
	private GenericCodeResponse createFlightSchedule(@RequestBody InsertFlightScheduleRequest request) {
		return flightScheduleService.createFlightSchedule(request);
	}

}
