CREATE TABLE dbo.airport(
	code varchar(50) NOT NULL,
	name varchar(50) NULL,
	country_code varchar(50) NULL,
 CONSTRAINT [PK_airport] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)
)

ALTER TABLE [dbo].[airport]  WITH CHECK ADD  CONSTRAINT [FK_airport_country] FOREIGN KEY([country_code])
REFERENCES [dbo].[country] ([code])