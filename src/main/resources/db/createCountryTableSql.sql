CREATE TABLE [dbo].[country](
	[code] [varchar](50) NOT NULL,
	[name] [varchar](50) NULL,
 CONSTRAINT [PK_country] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)
)

ALTER TABLE [dbo].[country]  WITH CHECK ADD  CONSTRAINT [FK_country_country] FOREIGN KEY([code])
REFERENCES [dbo].[country] ([code])
GO

ALTER TABLE [dbo].[country] CHECK CONSTRAINT [FK_country_country]
GO

ALTER TABLE [dbo].[country]  WITH CHECK ADD  CONSTRAINT [FK_country_country1] FOREIGN KEY([code])
REFERENCES [dbo].[country] ([code])
GO

ALTER TABLE [dbo].[country] CHECK CONSTRAINT [FK_country_country1]
GO