CREATE TABLE [dbo].[carrier_type](
	[name] [nvarchar](50) NULL,
	[code] [nvarchar](50) NULL,
	[capacity] [smallint] NULL,
	[description] [nvarchar](100) NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [pk_carrier_type] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)
)