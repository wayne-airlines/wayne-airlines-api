CREATE TABLE [dbo].[flight_schedule](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[origin_airport_code] [nvarchar](50) NOT NULL,
	[destination_airport_code] [nvarchar](50) NOT NULL,
	[flight_date_start] [datetime] NULL,
	[flight_date_finish] [datetime] NULL,
	[carrier_code] [nvarchar](50) NULL,
	[current_capacity] [smallint] NULL,
	[flight_type] [nvarchar](50) NULL,
	[status] [nvarchar](50) NULL,
 CONSTRAINT [pk_flight_schedule] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)
)