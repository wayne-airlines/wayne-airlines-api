CREATE TABLE [dbo].[carrier](
	[code] [nvarchar](50) NULL,
	[name] [nvarchar](50) NULL,
	[airport_code] [varchar](50) NULL,
	[carrier_type_code] [varchar](50) NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [pk_carrier] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)
)