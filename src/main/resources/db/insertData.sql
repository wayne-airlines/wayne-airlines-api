USE [WayneDB]
GO
INSERT [dbo].[country] ([code], [name]) VALUES (N'CAN', N'Canada')
GO
INSERT [dbo].[country] ([code], [name]) VALUES (N'CN', N'China')
GO
INSERT [dbo].[country] ([code], [name]) VALUES (N'HKG', N'Hongkong')
GO
INSERT [dbo].[country] ([code], [name]) VALUES (N'JPN', N'Japan')
GO
INSERT [dbo].[country] ([code], [name]) VALUES (N'PH', N'Philippines')
GO
INSERT [dbo].[airport] ([code], [name], [country_code]) VALUES (N'CRK', N'Clark International Airport', N'PH')
GO
INSERT [dbo].[airport] ([code], [name], [country_code]) VALUES (N'HIJ', N'Hiroshima Airport', N'JPN')
GO
INSERT [dbo].[airport] ([code], [name], [country_code]) VALUES (N'HKG', N'Hong Kong International Airport', N'HKG')
GO
INSERT [dbo].[airport] ([code], [name], [country_code]) VALUES (N'MNL', N'Ninoy Aquino International Airport', N'PH')
GO
INSERT [dbo].[airport] ([code], [name], [country_code]) VALUES (N'NGS', N'Nagasaki Airport', N'JPN')
GO
INSERT [dbo].[airport] ([code], [name], [country_code]) VALUES (N'NRT', N'Narita International Airport', N'JPN')
GO
INSERT [dbo].[airport] ([code], [name], [country_code]) VALUES (N'PPS', N'Puerto Princesa International Airport', N'PH')
GO
INSERT [dbo].[airport] ([code], [name], [country_code]) VALUES (N'PVG', N'Shanghai Pudong International Airport', N'CN')
GO
INSERT [dbo].[airport] ([code], [name], [country_code]) VALUES (N'SWA', N'Jieyang Chaoshan International Airport', N'CN')
GO
INSERT [dbo].[airport] ([code], [name], [country_code]) VALUES (N'YEG', N'Edmonton International Airport', N'CAN')
GO
INSERT [dbo].[airport] ([code], [name], [country_code]) VALUES (N'YVR', N'Vancouver International Airport', N'CAN')
GO
INSERT [dbo].[airport] ([code], [name], [country_code]) VALUES (N'YWG', N'Winnipeg International Airport', N'CAN')
GO
SET IDENTITY_INSERT [dbo].[carrier] ON 
GO
INSERT [dbo].[carrier] ([code], [name], [airport_code], [carrier_type_code], [id]) VALUES (N'GRAYSON', N'Grayson Carrier', N'MNL', N'PA', 1)
GO
INSERT [dbo].[carrier] ([code], [name], [airport_code], [carrier_type_code], [id]) VALUES (N'DRAKE', N'Drake Express', N'PVG', N'IEC', 2)
GO
INSERT [dbo].[carrier] ([code], [name], [airport_code], [carrier_type_code], [id]) VALUES (N'DAMIAN', N'Damian Lines', N'PPS', N'CAC', 3)
GO
INSERT [dbo].[carrier] ([code], [name], [airport_code], [carrier_type_code], [id]) VALUES (N'BARBARA', N'Barbara Pacific', N'HIJ', N'PA', 4)
GO
INSERT [dbo].[carrier] ([code], [name], [airport_code], [carrier_type_code], [id]) VALUES (N'JOKER', N'Joker Asylun', N'YVR', N'PA', 5)
GO
INSERT [dbo].[carrier] ([code], [name], [airport_code], [carrier_type_code], [id]) VALUES (N'TODD', N'Todd Express', N'NRT', N'IEC', 6)
GO
SET IDENTITY_INSERT [dbo].[carrier] OFF
GO
SET IDENTITY_INSERT [dbo].[carrier_type] ON 
GO
INSERT [dbo].[carrier_type] ([name], [code], [capacity], [description], [id]) VALUES (N'Passenger Airline', N'PA', 69, N'For passengers', 1)
GO
INSERT [dbo].[carrier_type] ([name], [code], [capacity], [description], [id]) VALUES (N'Integrated Express Carrier', N'IEC', 100, N'For packages', 2)
GO
INSERT [dbo].[carrier_type] ([name], [code], [capacity], [description], [id]) VALUES (N'Combination Aircraft Carrier', N'CAC', 200, N'For packages and passengers', 3)
GO
SET IDENTITY_INSERT [dbo].[carrier_type] OFF
GO
SET IDENTITY_INSERT [dbo].[flight_schedule] ON 
GO
INSERT [dbo].[flight_schedule] ([id], [code], [origin_airport_code], [destination_airport_code], [flight_date_start], [flight_date_finish], [carrier_code], [current_capacity], [flight_type], [status]) VALUES (1, N'JET-FLIGHT-TEST-01', N'MNL', N'PPS', CAST(N'2019-07-25T19:30:00.000' AS DateTime), CAST(N'2019-07-25T17:00:00.000' AS DateTime), N'GRAYSON', 69, N'DOMESTIC', N'FINISHED')
GO
INSERT [dbo].[flight_schedule] ([id], [code], [origin_airport_code], [destination_airport_code], [flight_date_start], [flight_date_finish], [carrier_code], [current_capacity], [flight_type], [status]) VALUES (2, N'JET-FLIGHT-TEST-02', N'PPS', N'MNL', CAST(N'2019-07-30T21:30:00.000' AS DateTime), CAST(N'2019-07-30T23:00:00.000' AS DateTime), N'GRAYSON', 10, N'DOMESTIC', N'CREATED')
GO
INSERT [dbo].[flight_schedule] ([id], [code], [origin_airport_code], [destination_airport_code], [flight_date_start], [flight_date_finish], [carrier_code], [current_capacity], [flight_type], [status]) VALUES (3, N'JET-FLIGHT-TEST-03', N'PPS', N'MNL', CAST(N'2019-08-28T23:30:00.000' AS DateTime), CAST(N'2019-08-29T23:00:00.000' AS DateTime), N'GRAYSON', 10, N'DOMESTIC', N'CREATED')
GO
SET IDENTITY_INSERT [dbo].[flight_schedule] OFF
GO
INSERT [dbo].[user] ([user_id], [username], [password], [role]) VALUES (1, N'j.torres', N'password', N'admin')
GO
INSERT [dbo].[user] ([user_id], [username], [password], [role]) VALUES (2, N'f.ramos', N'1234', N'admin')
GO
