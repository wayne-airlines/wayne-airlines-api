package com.jet.practice.WayneAirlinesAPI;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.LocalTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.jet.practice.WayneAirlinesAPI.entity.Airport;
import com.jet.practice.WayneAirlinesAPI.entity.FlightSchedule;
import com.jet.practice.WayneAirlinesAPI.entity.User;
import com.jet.practice.WayneAirlinesAPI.service.AirportService;
import com.jet.practice.WayneAirlinesAPI.service.FlightScheduleService;
import com.jet.practice.WayneAirlinesAPI.service.UserService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class WayneAirlinesApiApplicationTests {

	@Autowired
	private UserService userService;

	@Autowired
	private AirportService airportService;

	@Autowired
	private FlightScheduleService flightScheduleService;

	@Test
	public void contextLoads() {
	}

//	@Test
	public void testGetUser() {
		User user = userService.getUserByUsername("j.torres");
		assertNotNull(user);
		assertEquals("password", user.getPassword());
		assertEquals("admin", user.getRole());
	}

//	@Test
	public void testGetAllAirportByCountry() {
		List<Airport> airportList = airportService.getAllAirportsByCountry();
		System.out.println(airportList.toString());
	}

//	@Test
	public void testDateCompare() {
		FlightSchedule flightSchedule = flightScheduleService.getFlightScheduleByCode("JET-FLIGHT-TEST-01");
		Date beforeDate = flightSchedule.getFlightDateStart();
		Date afterDate = flightSchedule.getFlightDateFinish();
		Boolean result = beforeDate.before(afterDate);
		LocalTime beforeTime = new LocalTime(beforeDate.getTime());
		System.out.println(beforeTime);
		System.out.println(result);
		assertTrue(result);
	}

	//@Test
	public void testGetLatest() {
		try {
			FlightSchedule flightSchedule = flightScheduleService.getLatestFlightSchedule("GRAYSON");
			System.out.println(flightSchedule.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

//	@Test
	public void testSomething() {
		List<FlightSchedule> flightList = flightScheduleService.getFlightScheduleByCarrierCode("GRAYSON");
		FlightSchedule latestFlight = Collections.max(flightList, Comparator.comparing(f -> f.getFlightDateFinish()));
		System.out.println(latestFlight.toString());
	}
	
	@Test
	public void testGetByDate() {
		FlightSchedule flightSchedule = flightScheduleService.getFlightScheduleByCode("JET-FLIGHT-TEST-01");
		Date beforeDate = flightSchedule.getFlightDateStart();
		DateTime dateToQuery = new DateTime(beforeDate);
		System.out.println(dateToQuery.toString("yyyy-MM-dd"));
		List<FlightSchedule> flightList = flightScheduleService.getFlightScheduleByFlightStartDate(dateToQuery.toString("yyyy-MM-dd"));
		//List<FlightSchedule>flightList = flightScheduleService.getFlightScheduleByFlightStartDate("2019-07-25");
		System.out.println(flightList.toString());
	}

}
